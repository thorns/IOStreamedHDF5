 /*@@
   @header    ioStreamedHDF5GH.h
   @date      Jun 20 2000
   @author    Thomas Radke
   @desc
              The GH extensions structure for IOStreamedHDF5.
   @enddesc
   @version   $Header$
 @@*/

#ifndef _IOSTREAMEDHDF5_IOSTREAMEDHDF5GH_H_
#define _IOSTREAMEDHDF5_IOSTREAMEDHDF5GH_H_ 1

#include "SocketUtils.h"
#include "CactusPUGHIO/IOHDF5Util/src/ioHDF5UtilGH.h"


/* IOStreamedHDF5 GH extension structure */
typedef struct
{
  /* default number of times to output */
  int out_every_default;

  /* number of times to output for each variable */
  CCTK_INT *out_every;

  /* the last iteration output for each variable */
  int *out_last;

  /* list of variables to output */
  char *out_vars;

  /* I/O request description list (for all CCTK variables) */
  ioRequest **requests;

  /* ports to output data and checkpoint files to */
  unsigned int data_port, checkpoint_port;

  /* sockets to output data and checkpoint files to */
  SOCKET data_socket, checkpoint_socket;

  /* file access property list used to write checkpoint files */
  hid_t checkpoint_fapl;

  /* flag indicating whether we want timing info on checkpointing/recovery
     and corresponding array of Cactus timers */
  int print_timing_info;
  int timers[IOHDF5_NUM_TIMERS];

  /* filename for advertising the hostname / port number information */
  char advertised_filename[L_tmpnam];

  /* stop on I/O parameter parsing errors ? */
  int stop_on_parse_errors;

} ioStreamedHDF5GH;

#ifdef __cplusplus
extern "C"
{
#endif

/* prototypes of functions to be registered as IOStreamedHDF5's I/O method */
int IOStreamedHDF5_OutputGH (const cGH *GH);
int IOStreamedHDF5_TriggerOutput (const cGH *GH, int);
int IOStreamedHDF5_TimeFor (const cGH *GH, int);
int IOStreamedHDF5_OutputVarAs (const cGH *GH, const char *var, const char *alias);
int IOStreamedHDF5_Recover (cGH *GH, const char *basefilename, int called_from);

/* other function prototypes */
int IOStreamedHDF5_Write (const cGH *GH, int vindex, const char *alias);
void IOStreamedHDF5_CheckSteerableParameters (const cGH *GH,
                                              ioStreamedHDF5GH *myGH);

#ifdef __cplusplus
} // extern "C"
#endif

#endif  /* _IOSTREAMEDHDF5_IOSTREAMEDHDF5GH_H_ */
