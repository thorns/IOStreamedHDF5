 /*@@
   @file      DumpGH.c
   @date      Fri Oct 6 2000
   @author    Thomas Radke
   @desc 
              Checkpoint routines scheduled at CCTK_CPINITIAL, CCTK_CHECKPOINT,
              and CCTK_TERMINATE.
              They check the IO checkpointing parameters and - if it's time
              to do so - call the routine which finally creates a checkpoint.
   @enddesc 
   @version   $Id$
 @@*/


#include "cctk.h"
#include "cctk_Parameters.h"
#include "CactusBase/IOUtil/src/ioGH.h"
#include "CactusPUGH/PUGH/src/include/pugh.h"
#include "CactusBase/IOUtil/src/ioutil_CheckpointRecovery.h"
#include "CactusPUGHIO/IOHDF5Util/src/ioHDF5UtilGH.h"
#include "ioStreamedHDF5GH.h"

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOStreamedHDF5_DumpGH_c)


/********************************************************************
 ********************    External Routines   ************************
 ********************************************************************/
void IOStreamedHDF5_InitialDataCheckpoint (const cGH *GH);
void IOStreamedHDF5_EvolutionCheckpoint (const cGH *GH);
void IOStreamedHDF5_TerminationCheckpoint (const cGH *GH);


/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
static int Checkpoint (const cGH *GH, int called_from);


 /*@@
   @routine    IOStreamedHDF5_InitialDataCheckpoint
   @date       Fri Oct 6 2000
   @author     Thomas Radke
   @desc 
               This routine is registered at CCTK_CPINITIAL.
               It checks if initial data should be checkpointed.
   @enddesc 

   @calls      Checkpoint

   @var        GH
   @vdesc      Pointer to CCTK grid hierarchy to be checkpointed
   @vtype      const cGH *
   @vio        in
   @endvar
@@*/
void IOStreamedHDF5_InitialDataCheckpoint (const cGH *GH)
{
  DECLARE_CCTK_PARAMETERS


  if (checkpoint && checkpoint_ID)
  {
    Checkpoint (GH, CP_INITIAL_DATA);
  }
}


 /*@@
   @routine    IOStreamedHDF5_EvolutionCheckpoint
   @date       Fri Oct 6 2000
   @author     Thomas Radke
   @desc 
               This routine is registered at CCTK_CHECKPOINT.
               It periodically checks if it's time to checkpoint evolution data.
   @enddesc 
 
   @calls      Checkpoint

   @var        GH
   @vdesc      Pointer to CCTK grid hierarchy to be checkpointed
   @vtype      const cGH *
   @vio        in
   @endvar
@@*/
void IOStreamedHDF5_EvolutionCheckpoint (const cGH *GH)
{
  DECLARE_CCTK_PARAMETERS


  if (checkpoint &&
      ((checkpoint_every > 0 && GH->cctk_iteration % checkpoint_every == 0) ||
       checkpoint_next))
  {
    if (CCTK_Equals (verbose, "full"))
    {
      CCTK_INFO("------------------------------------------------------------");
      CCTK_VInfo (CCTK_THORNSTRING, "Dumping periodic checkpoint file at "
                  "iteration %d", GH->cctk_iteration);
    }
    Checkpoint (GH, CP_EVOLUTION_DATA);

    /* reset the 'checkpoint_next' parameter */
    if (checkpoint_next)
    {
      CCTK_ParameterSet ("checkpoint_next", CCTK_THORNSTRING, "no");
    }
  }
}


 /*@@
   @routine    IOStreamedHDF5_TerminationCheckpoint
   @date       Fri Oct 6 2000
   @author     Thomas Radke
   @desc 
               This routine is registered at CCTK_TERMINATE.
               It checks if the last iteration should be checkpointed.
   @enddesc 

   @calls      Checkpoint
 
   @var        GH
   @vdesc      Pointer to CCTK grid hierarchy to be checkpointed
   @vtype      const cGH *
   @vio        in
   @endvar
@@*/
void IOStreamedHDF5_TerminationCheckpoint (const cGH *GH)
{
  DECLARE_CCTK_PARAMETERS


  if (checkpoint && checkpoint_on_terminate)
  {
    Checkpoint (GH, CP_EVOLUTION_DATA);
  }
}


/********************************************************************
 ********************    Internal Routines   ************************
 ********************************************************************/
 /*@@
   @routine    Checkpoint
   @date       Fri Oct 6 2000
   @author     Thomas Radke
   @desc 
               The heart of checkpointing.
               Called by the different wrappers, this routine creates
               a new checkpoint file and then dumps away using the
               dump routines from IOHDF5Util.
   @enddesc 

   @calls      IOHDF5Util_DumpGH

   @var        GH
   @vdesc      Pointer to CCTK grid hierarchy to be checkpointed
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        called_from
   @vdesc      flag indicating where this routine was called from
               (either CHECKPOINT_ID, CHECKPOINT, or filereader)
   @vtype      int
   @vio        in
   @endvar
   @returntype int
   @returndesc
               -1 if checkpoint file could not be created
               or returncode of @seeroutine IOUtilHDF5_DumpGH 
   @endreturndesc
@@*/
static int Checkpoint (const cGH *GH, int called_from)
{
  hid_t file;
  int old_ioproc, old_nioprocs, old_ioproc_every;
  fd_set readset;
  struct timeval timeout;
  ioGH *ioUtilGH;
  ioStreamedHDF5GH *myGH;
  CCTK_INT4 retval;
  const char *timer_descriptions[3] = {"Time to dump parameters: ",
                                       "Time to dump datasets:   ",
                                       "Total time to checkpoint:"};
  DECLARE_CCTK_PARAMETERS


  /* suppress compiler warning about unused variables */
  (void) (called_from + 0);

  retval = 0;
  file = -1;

  ioUtilGH = (ioGH *) CCTK_GHExtension (GH, "IO");
  myGH = (ioStreamedHDF5GH *) CCTK_GHExtension (GH, "IOStreamedHDF5");

  /* check if IOStreamedHDF5 was registered as I/O method */
  if (myGH == NULL)
  {
    CCTK_WARN (2, "No IOStreamedHDF5 I/O method registered");
    return (-1);
  }

  /* start the CP_TOTAL_TIMER timer */
  if (myGH->print_timing_info)
  {
    CCTK_TimerStartI (myGH->timers[CP_TOTAL_TIMER]);
  }

  /* for now we can only have I/O mode "oneproc" */
  if (ioUtilGH->nioprocs != 1)
  {
    CCTK_WARN (2, "Output into multiple chunked files not yet implemented. "
                  "Switching to I/O mode \"oneproc\".");
  }
  old_ioproc = ioUtilGH->ioproc;
  ioUtilGH->ioproc = 0;
  old_nioprocs = ioUtilGH->nioprocs;
  ioUtilGH->nioprocs = 1;
  old_ioproc_every = ioUtilGH->ioproc_every;
  ioUtilGH->ioproc_every = CCTK_nProcs (GH);

  /* now open the file */
  if (CCTK_MyProc (GH) == ioUtilGH->ioproc &&
      myGH->checkpoint_socket != INVALID_SOCKET)
  {
    if (CCTK_Equals (verbose, "full"))
    {
      CCTK_VInfo (CCTK_THORNSTRING, "Creating checkpoint file");
    }

    file = H5Fcreate ("dummy_name", H5F_ACC_TRUNC, H5P_DEFAULT,
                      myGH->checkpoint_fapl);
    if (file < 0)
    {
      CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,
                  "Can't create checkpoint file on port %u. "
                  "Checkpointing is skipped.", myGH->checkpoint_port);
      retval = -1;
    }
  }
  else
  {
    retval = -1;
  }

#ifdef CCTK_MPI
  /* broadcast the result of the H5Fcreate() call */
  CACTUS_MPI_ERROR (MPI_Bcast (&retval, 1, PUGH_MPI_INT4, 0,
                               PUGH_pGH (GH)->PUGH_COMM_WORLD));
#endif

  if (retval == 0)
  {
    retval = IOHDF5Util_DumpGH (GH,
                                myGH->print_timing_info ? myGH->timers : NULL,
                                file);
  }

  if (file >= 0)
  {
    if (checkpoint_accept_timeout > 0)
    {
      if (CCTK_Equals (verbose, "full"))
      {
        CCTK_VInfo (CCTK_THORNSTRING, "Waiting %d seconds to send "
                                      "checkpoint file to port %u",
                                      checkpoint_accept_timeout,
                                      myGH->checkpoint_port);
      }

      FD_ZERO (&readset);
      FD_SET (myGH->checkpoint_socket, &readset);
      timeout.tv_sec = checkpoint_accept_timeout;
      timeout.tv_usec = 0;
      select (myGH->checkpoint_socket + 1, &readset, NULL, NULL, &timeout);
    }
    if (CCTK_Equals (verbose, "full"))
    {
      CCTK_VInfo (CCTK_THORNSTRING, "Closing checkpoint file on port %d",
                  myGH->checkpoint_port);
    }
    HDF5_ERROR (H5Fclose (file));
  }

  /* restore original I/O mode */
  ioUtilGH->ioproc = old_ioproc;
  ioUtilGH->nioprocs = old_nioprocs;
  ioUtilGH->ioproc_every = old_ioproc_every;

  /* stop the CP_TOTAL_TIMER timer and print timing information */
  if (myGH->print_timing_info)
  {
    CCTK_TimerStopI (myGH->timers[CP_TOTAL_TIMER]);
    IOUtil_PrintTimings ("Timing information for checkpointing "
                         "with IOStreamedHDF5:",
                         3, myGH->timers, timer_descriptions);
  }

  return (retval);
}
