 /*@@
   @file      Output.c
   @date      Tue Jan 9 1999
   @author    Gabrielle Allen
   @desc
              Functions to deal with streaming output of variables
              in HDF5 format.
   @enddesc
   @version   $Id$
 @@*/


#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "util_String.h"
#include "ioStreamedHDF5GH.h"

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOStreamedHDF5_Output_c)


/*@@
   @routine    IOStreamedHDF5_OutputGH
   @date       Sat March 6 1999
   @author     Gabrielle Allen
   @desc
               Loops over all variables and outputs them if necessary
   @enddesc

   @calls      IOStreamedHDF5_TimeFor
               IOStreamedHDF5_Write

   @var        GH
   @vdesc      Pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar

   @returntype int
   @returndesc
               the number of variables which were output at this iteration
               (or 0 if it wasn't time to output yet)
   @endreturndesc
@@*/
int IOStreamedHDF5_OutputGH (const cGH *GH)
{
  int vindex, retval;
  const ioStreamedHDF5GH *myGH;


  retval = 0;
  myGH = CCTK_GHExtension (GH, "IOStreamedHDF5");

  /* loop over all variables */
  for (vindex = CCTK_NumVars () - 1; vindex >= 0; vindex--)
  {
    if (IOStreamedHDF5_TimeFor (GH, vindex) &&
        IOStreamedHDF5_Write (GH, vindex, CCTK_VarName (vindex)) == 0)
    {
      /* register variable as having output this iteration */
      myGH->out_last[vindex] = GH->cctk_iteration;
      retval++;
    }
  }

  return (retval);
}


/*@@
   @routine    IOStreamedHDF5_OutputVarAs
   @date       Sat March 6 1999
   @author     Gabrielle Allen
   @desc
               Unconditional output of a variable using
               the IOStreamedHDF5 I/O method.
   @enddesc

   @calls      IOStreamedHDF5_Write

   @var        GH
   @vdesc      Pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        fullname
   @vdesc      complete name of variable to output
   @vtype      const char *
   @vio        in
   @endvar
   @var        alias
   @vdesc      alias name of variable to output
               (used to generate output filename)
   @vtype      const char *
   @vio        in
   @endvar

   @returntype int
   @returndesc
               return code of @seeroutine IOStreamedHDF5_Write
   @endreturndesc
@@*/
int IOStreamedHDF5_OutputVarAs (const cGH *GH, const char *fullname,
                                const char *alias)
{
  int vindex, retval;
  DECLARE_CCTK_PARAMETERS


  vindex = CCTK_VarIndex (fullname);

  if (CCTK_Equals (verbose, "full"))
  {
    CCTK_VInfo (CCTK_THORNSTRING, "IOStreamedHDF5_OutputVarAs: "
                                  "output of variable (fullname, alias) = "
                                  "('%s', '%s')", fullname, alias);
  }

  /* do the output */
  retval = IOStreamedHDF5_Write (GH, vindex, alias);

  return (retval);
}


/*@@
   @routine    IOStreamedHDF5_TimeFor
   @date       Sat March 6 1999
   @author     Gabrielle Allen
   @desc
               Decides if it is time to output a variable
               using the IOStreamedHDF5 I/O method.
   @enddesc

   @calls      IOStreamedHDF5_CheckSteerableParameters

   @var        GH
   @vdesc      Pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        vindex
   @vdesc      index of variable
   @vtype      int
   @vio        in
   @endvar

   @returntype int
   @returndesc
               1 if output should take place at this iteration, or<BR>
               0 if not
   @endreturndesc
@@*/
int IOStreamedHDF5_TimeFor (const cGH *GH, int vindex)
{
  int retval;
  char *fullname;
  ioStreamedHDF5GH *myGH;


  myGH = CCTK_GHExtension (GH, "IOStreamedHDF5");
  IOStreamedHDF5_CheckSteerableParameters (GH, myGH);

  /* check if this variable should be output */
  retval = myGH->requests[vindex] && myGH->requests[vindex]->out_every > 0 &&
           GH->cctk_iteration % myGH->requests[vindex]->out_every == 0;
  if (retval)
  {
    /* check if variable was not already output this iteration */
    if (myGH->out_last[vindex] == GH->cctk_iteration)
    {
      fullname = CCTK_FullName (vindex);
      CCTK_VWarn (6, __LINE__, __FILE__, CCTK_THORNSTRING,
                  "Already done IOStreamedHDF5 output for variable '%s' in "
                  "current iteration (probably via triggers)", fullname);
      free (fullname);
      retval = 0;
    }
  }

  return (retval);
}


/*@@
   @routine    IOStreamedHDF5_TriggerOutput
   @date       Sat March 6 1999
   @author     Gabrielle Allen
   @desc
               Triggers the output of a variable
               using the IOStreamedHDF5 I/O method.
   @enddesc

   @calls      IOStreamedHDF5_Write

   @var        GH
   @vdesc      Pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        vindex
   @vdesc      index of variable
   @vtype      int
   @vio        in
   @endvar

   @returntype int
   @returndesc
               return code of @seeroutine IOStreamedHDF5_Write
   @endreturndesc
@@*/
int IOStreamedHDF5_TriggerOutput (const cGH *GH, int vindex)
{
  int retval;
  ioStreamedHDF5GH *myGH;
  char *fullname;
  const char *varname;
  DECLARE_CCTK_PARAMETERS


  varname = CCTK_VarName (vindex);

  myGH = CCTK_GHExtension (GH, "IOStreamedHDF5");

  if (CCTK_Equals (verbose, "full"))
  {
    fullname = CCTK_FullName (vindex);
    CCTK_VInfo (CCTK_THORNSTRING, "IOStreamedHDF5_TriggerOutput: "
                                  "output of (varname, fullname) = "
                                  "('%s', '%s')", varname, fullname);
    free (fullname);
  }

  /* do the output */
  retval = IOStreamedHDF5_Write (GH, vindex, varname);

  if (retval == 0)
  {
    /* register variable as having output this iteration */
    myGH->out_last[vindex] = GH->cctk_iteration;
  }

  return (retval);
}


/*@@
   @routine    IOStreamedHDF5_CheckSteerableParameters
   @date       Mon Oct 10 2000
   @author     Thomas Radke
   @desc
               Checks if IOStreamedHDF5 steerable parameters were changed
               and does appropriate re-evaluation.
   @enddesc

   @calls      IOHDF5Util_ParseVarsForOutput

   @var        GH
   @vdesc      Pointer to CCTK grid hierarchy
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        myGH
   @vdesc      Pointer to IOStreamedHDF5 grid hierarchy
   @vtype      ioStreamedHDF5GH *
   @vio        inout
   @endvar
@@*/
void IOStreamedHDF5_CheckSteerableParameters (const cGH *GH,
                                              ioStreamedHDF5GH *myGH)
{
  int i;
  char *fullname, *msg;
  DECLARE_CCTK_PARAMETERS


  /* how often to output */
  i = myGH->out_every_default;
  myGH->out_every_default = out_every >= 0 ? out_every : io_out_every;

  /* report if frequency changed */
  if (myGH->out_every_default != i && ! CCTK_Equals (verbose, "none"))
  {
    if (myGH->out_every_default > 0)
    {
      CCTK_VInfo (CCTK_THORNSTRING, "Periodic streamed HDF5 output every %d "
                  "iterations", myGH->out_every_default);
    }
    else
    {
      CCTK_INFO ("Periodic streamed HDF5 output turned off");
    }
  }

  /* re-parse the 'IOStreamedHDF5::out_vars' parameter if it was changed */
  if (strcmp (out_vars, myGH->out_vars) || myGH->out_every_default != i)
  {
    IOUtil_ParseVarsForOutput (GH, CCTK_THORNSTRING, "IOStreamedHDF5::out_vars",
                               myGH->stop_on_parse_errors, out_vars,
                               myGH->out_every_default, -1.0, myGH->requests);

    if (myGH->out_every_default == i || ! CCTK_Equals (verbose, "none"))
    {
      msg = NULL;
      for (i = CCTK_NumVars () - 1; i >= 0; i--)
      {
        if (myGH->requests[i])
        {
          fullname = CCTK_FullName (i);
          if (! msg)
          {
            Util_asprintf (&msg, "Periodic streamed HDF5 output requested "
                           "for '%s'", fullname);
          }
          else
          {
            char *tmp = msg;
            Util_asprintf (&msg, "%s, '%s'", msg, fullname);
            free (tmp);
          }
          free (fullname);
        }
      }
      if (msg)
      {
        CCTK_INFO (msg);
        free (msg);
      }
    }

    /* save the last setting of 'IOStreamedHDF5::out_vars' parameter */
    free (myGH->out_vars);
    myGH->out_vars = strdup (out_vars);
  }
}
