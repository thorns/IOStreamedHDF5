/*@@
   @file      Write.c
   @date      Tue Oct 10 2000
   @author    Thomas Radke
   @desc
              File handling routines for IOStreamedHDF5.
   @enddesc
   @version   $Id$
 @@*/


#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "CactusBase/IOUtil/src/ioGH.h"
#include "CactusPUGHIO/IOHDF5Util/src/ioHDF5UtilGH.h"
#include "ioStreamedHDF5GH.h"

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOStreamedHDF5_Write_c)


/*@@
   @routine    IOStreamedHDF5_Write
   @date       Tue Oct 10 2000
   @author     Thomas Radke
   @desc
               Opens the HDF5 output file, calls the dump routine,
               and closes it again.
   @enddesc

   @calls      IOHDF5Util_DumpVar

   @var        GH
   @vdesc      Pointer to CCTK GH
   @vtype      const cGH *
   @vio        in
   @endvar
   @var        vindex
   @vdesc      index of variable
   @vtype      int
   @vio        in
   @endvar
   @var        alias
   @vdesc      alias name of variable to output
   @vtype      const char *
   @vio        unused
   @endvar

   @returntype int
   @returndesc
               return code of @seeroutine IOHDF5Util_DumpVar, or<BR>
               -1 if variable has no storage assigned
   @endreturndesc
@@*/
int IOStreamedHDF5_Write (const cGH *GH, int vindex, const char *alias)
{
  int old_ioproc, old_nioprocs, old_ioproc_every, retval;
  char *fullname;
  ioGH *ioUtilGH;
  ioStreamedHDF5GH *myGH;
  hid_t file, plist;
  H5FD_stream_fapl_t fapl;
  DECLARE_CCTK_PARAMETERS


  /* suppress compiler warnings about unused variables */
  (void) (alias + 0);

  /* first, check if variable has storage assigned */
  if (! CCTK_QueryGroupStorageI (GH, CCTK_GroupIndexFromVarI (vindex)))
  {
    fullname = CCTK_FullName (vindex);
    CCTK_VWarn (2, __LINE__, __FILE__, CCTK_THORNSTRING,
                "No IOStreamedHDF5 output for '%s' (no storage)", fullname);
    free (fullname);
    return (-1);
  }

  ioUtilGH = CCTK_GHExtension (GH, "IO");
  myGH = CCTK_GHExtension (GH, "IOStreamedHDF5");

  /* for now we can only have I/O mode "oneproc" */
  if (ioUtilGH->nioprocs != 1)
  {
    CCTK_WARN (2, "Output into multiple chunked files not yet implemented. "
                  "Switching to IO mode \"oneproc\".");
  }
  old_ioproc = ioUtilGH->ioproc;
  ioUtilGH->ioproc = 0;
  old_nioprocs = ioUtilGH->nioprocs;
  ioUtilGH->nioprocs = 1;
  old_ioproc_every = ioUtilGH->ioproc_every;
  ioUtilGH->ioproc_every = CCTK_nProcs (GH);

  file = -1;
  if (CCTK_MyProc (GH) == 0 && myGH->data_socket != INVALID_SOCKET)
  {
    if (CCTK_Equals (verbose, "full"))
    {
      CCTK_VInfo (CCTK_THORNSTRING, "Opening HDF5 output file "
                                    "on data output port %u", myGH->data_port);
    }

    /* set file access property list to use the Stream VFD
       and open the file */
    fapl.increment = 0;
    fapl.socket = myGH->data_socket;
    fapl.do_socket_io = 1;
    fapl.backlog = 5;
    fapl.broadcast_fn = NULL;
    fapl.broadcast_arg = NULL;

    HDF5_ERROR (plist = H5Pcreate (H5P_FILE_ACCESS));
    HDF5_ERROR (H5Pset_fapl_stream (plist, &fapl));

    /* filename is not used if we pass a plist
       but it must not be NULL or an empty string */
    HDF5_ERROR (file = H5Fcreate ("unused", H5F_ACC_TRUNC, H5P_DEFAULT,
                                    plist));
    HDF5_ERROR (H5Pclose (plist));
  }

  /* output the data */
  retval = IOHDF5Util_DumpVar (GH, myGH->requests[vindex], file);

  /* attach global information */
  if (! retval && file >= 0)
  {
    IOHDF5Util_DumpGHExtensions (GH, file);
  }

  /* close the file */
  if (file >= 0)
  {
    if (CCTK_Equals (verbose, "full"))
    {
      CCTK_INFO ("Closing HDF5 output file from this iteration");
    }
    HDF5_ERROR (H5Fclose (file));
  }

  /* restore original IO mode */
  ioUtilGH->ioproc = old_ioproc;
  ioUtilGH->nioprocs = old_nioprocs;
  ioUtilGH->ioproc_every = old_ioproc_every;

  return (retval);
}
